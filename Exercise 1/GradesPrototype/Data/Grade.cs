﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace GradesPrototype.Data
{
    // Types of user
    public enum Role { Teacher, Student };

    // WPF Databinding requires properties

    // TODO: Exercise 1: Task 1a: Convert Grade into a class and define constructors
    public class Grade
    {
        public int StudentID { get; set; }
        public string AssessmentDate { get; set; }
        public string SubjectName { get; set; }
        public string Assessment { get; set; }
        public string Comments { get; set; }

        public Grade()
        {
            StudentID = 0;
            AssessmentDate = string.Empty;
            SubjectName = string.Empty;
            Assessment = string.Empty;
            Comments = string.Empty;
        }
    }

    // TODO: Exercise 1: Task 2a: Convert Student into a class, make the password property write-only, add the VerifyPassword method, and define constructors
    public class Student
    {
        public int StudentID { get; set; }
        public string UserName { get; set; }
        public string Password { private get; set; }
        public int TeacherID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public Student()
        {
            StudentID = 0;
            UserName = string.Empty;
            Password = string.Empty;
            TeacherID = 0;
            FirstName = string.Empty;
            LastName = string.Empty;
        }

        public bool VerifyPassword(string username, string password)
        {
            return String.Compare(UserName, username) == 0 && String.Compare(Password, password) == 0;
        }

        public string GetPassword()
        {
            return Password;
        }
    }

    // TODO: Exercise 1: Task 2b: Convert Teacher into a class, make the password property write-only, add the VerifyPassword method, and define constructors
    public class Teacher
    {
        public int TeacherID { get; set; }
        public string UserName { get; set; }
        public string Password { private get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Class { get; set; }

        public Teacher()
        {
            TeacherID = 0;
            UserName = string.Empty;
            Password = string.Empty;
            FirstName = string.Empty;
            LastName = string.Empty;
            Class = string.Empty;
        }

        public bool VerifyPassword(string username, string password)
        {
            return String.Compare(UserName, username) == 0 && String.Compare(Password, password) == 0;
        }

        public string GetPassword()
        {
            return Password;
        }
    }
}
