using GradesPrototype.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace GradesTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestInitialize]
        public void Init()
        {
            DataSource.CreateData();
        }

        [TestMethod]
        public void TestValidGrade()
        {
            try
            {
                Grade grade = new Grade(5, "3-20-2021", "Math", "A+", "Good job");
            }
            catch(Exception ex)
            {
                Assert.Fail("Expected no exception, but got: " + ex.Message);
            }
        }
         
        [TestMethod]
        public void TestBadDate()
        {
            Grade grade = new Grade();
            Assert.ThrowsException<Exception>(() => grade.AssessmentDate = "20-13-28");
        }

        [TestMethod]
        public void TestDateNotRecognized()
        {
            Grade grade = new Grade();
            Assert.ThrowsException<Exception>(() => grade.AssessmentDate = "1-4-2022");
        }

        [TestMethod]
        public void TestBadAssessment()
        {
            Grade grade = new Grade();
            Assert.ThrowsException<Exception>(() => grade.Assessment = "HA");
        }

        [TestMethod]
        public void TestBadSubject()
        {
            Grade grade = new Grade();
            Assert.ThrowsException<Exception>(() => grade.SubjectName = "Hello");
        }

    }
}
